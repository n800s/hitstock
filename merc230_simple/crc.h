#ifndef _CRC_H

#define _CRC_H

uint16_t UpdCRC(uint8_t c, uint16_t oldCRC);

uint16_t calcCRC(uint8_t *BufSend, int LengthSend);

uint8_t *addCRCbytes(uint8_t *BufSend, int LengthSend);

#endif // _CRC_H
