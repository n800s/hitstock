#include "all.h"
#include "config.h"
#include "exc.h"
#include "merc230.h"

static char serdev[256] = SERIAL_DEVICE;
static int baudrate = BAUDRATE;
static char destdir[256] = ".";
static uint8_t address = 0;
static char cmdfname[256] = "";

static char tmpfname[392];
static char destfname[392];
static FILE *dfile;


static void usage()
{
	printf("merc230 <options>\n");
	printf("-h - Help\n");
	printf("-p <Destination directory (default: current)>\n");
	printf("-a <Mercury 230 address (default: 0)>\n");
	printf("-d <Serial device (default: %s)>\n", serdev);
	printf("-b <Baud rate (default: %d)>\n", baudrate);
	printf("-c <Command file>\n");
}

static void err_usage(char arg)
{
	printf("Bad arg \"%c\"", arg);
	usage();
}

bool open_file()
{
	// create transaction file
	time_t t = time(NULL);
	char fname[128];
	strftime(fname, sizeof(fname), "__%Y_%m_%d_%H_%M_%S.json", localtime(&t));
	snprintf(tmpfname, sizeof(tmpfname), "%s/%s", destdir, fname);
	strftime(fname, sizeof(fname), "%Y_%m_%d_%H_%M_%S.json", localtime(&t));
	snprintf(destfname, sizeof(destfname), "%s/%s", destdir, fname);
//	printf("rename %s to %s\n", tmpfname, destfname);
	dfile = fopen(tmpfname, "wt");
	if(dfile) {
		fprintf(dfile, "[\n");
		fprintf(dfile, "\t{\n");
		fprintf(dfile, "\t\t\"type\": \"start\"\n");
		fprintf(dfile, "\t\t\"timestamp\": %d\n", (int)time(NULL));
		fprintf(dfile, "\t},\n");
	}
	return dfile != NULL;
}

bool close_file()
{
	int rs = true;
	if(dfile) {
		fprintf(dfile, "\t{\n");
		fprintf(dfile, "\t\"type\": \"finish\",\n");
		fprintf(dfile, "\t\t\"timestamp\": %d\n", (int)time(NULL));
		fprintf(dfile, "\t}\n");
		fprintf(dfile, "]\n");
		fclose(dfile);
	// rename file
		rs = rename(tmpfname, destfname) == 0;
	}
	return rs;
}

double to_number(const JSONValue *val)
{
	return val->AsNumber();
}

int to_integer(const JSONValue *val)
{
	int rs;
	if(val->IsNumber()) {
		rs = to_number(val);
	} else {
		std::wstringstream converter(val->AsString());
		converter >> std::hex >> rs;
	}
	return rs;
}

// every 15 min, every day, the first day of month
int main(int argc, char* argv[])
{
	int ch;
	bool rs = false;
	while((ch = getopt(argc, argv, "p:a:d:b:c:")) != -1)
	{
		switch(ch)
		{
			case 'p':
				strncpy(destdir, optarg, sizeof(destdir) - 1);
				break;
			case 'a':
				address = atoi(optarg);
				break;
			case 'd':
				strncpy(serdev, optarg, sizeof(serdev));
				break;
			case 'b':
				baudrate = atoi(optarg);
				break;
			case 'c':
				strncpy(cmdfname, optarg, sizeof(cmdfname));
				break;
			case 'h':
				usage();
				break;
			default:
				err_usage(ch);
				break;
		}
	}

	try {
		FILE *cmdfile = fopen(cmdfname, "r");
		long fsize = 0;
		char *sjson;
		if(cmdfile) {
			fseek(cmdfile, 0, SEEK_END);
			fsize = ftell(cmdfile);
			fseek(cmdfile, 0, SEEK_SET);

			sjson = (char *)malloc(fsize + 1);
			fread(sjson, fsize, 1, cmdfile);
			fclose(cmdfile);

		} else {
			throw merception("error reading config file...%s", strerror(errno));
			fsize = 0;
			sjson = (char *)malloc(fsize + 1);
		}
		sjson[fsize] = 0;

		Merc230 *m = new Merc230(serdev, address, baudrate);
		if(open_file()) {
			JSONValue *val = JSON::Parse(sjson);
			if (val == NULL)
			{
				throw merception("Error config file parsing\n");
			}
			else
			{
				if (val->IsArray())
				{
					m->open_channel();
					const JSONArray &root = val->AsArray();
					for (unsigned int i = 0; i < root.size(); i++)
					{
						m->debug = true;
						if(root[i]->IsObject()) {
							JSONObject cmd = root[i]->AsObject();
							std::wstring type = cmd[L"type"]->AsString();
							printf("$$$ %ls $$$\n", type.c_str());
							if( type == L"instant_current" ) {
								m->get_current_params(dfile, to_number(cmd[L"phase"]));
							} else if( type == L"instant_voltage" ) {
								m->get_voltage_params(dfile, to_number(cmd[L"phase"]));
							} else if ( type == L"instant_power" ) {
								int inum = 0;
								std::wstring num = cmd[L"num"]->AsString();
								if( num == L"P" ) {
									inum = 0;
								} else if( num == L"Q" ) {
									inum = 1;
								} else if( num == L"S" ) {
									inum = 2;
								}
								m->get_power_params(dfile, inum, to_number(cmd[L"phase"]));
							} else if ( type == L"get_array" ) {
								m->get_regarray(dfile, to_number(cmd[L"array_num"]), to_number(cmd[L"month"]), to_number(cmd[L"tariff"]));
							} else if ( type == L"current_time" ) {
								m->get_current_time(dfile);
							} else if ( type == L"raw_request" ) {
								const JSONArray &va = cmd[L"bytes"]->AsArray();
								uint8_t *vbuf = new uint8_t[va.size()];
								int vi = 0;
								for (unsigned int j = 0; j < va.size(); j++)
								{
									vbuf[vi++] = to_integer(va[j]);
								}
								m->raw_request(dfile, to_integer(cmd[L"request_code"]), vbuf, va.size());
								delete vbuf;
							} else {
								std::wcout << "not found: " << cmd[L"type"]->AsString() << std::endl;
							}
						} else {
							throw merception("Error config file parsing %s as object\n", root[i]->AsString().c_str());
						}
//						m->debug = false;
					}
					m->close_channel();
					rs = true;
					if(!close_file()) {
						fprintf(stderr, "Can not rename temporary file\n");
						rs = false;
					}
				}
				delete val;
			}
		} else {
			fprintf(stderr, "Can not create temporary file\n");
		}
	} catch(merception &e) {
		if(dfile) {
			fprintf(dfile, "{\n");
			fprintf(dfile, "\t\"type\": \"fatal_error\"\n");
			fprintf(dfile, "\t\"error\": \"%s\"\n", e.what());
			fprintf(dfile, "}\n");
			if(!close_file()) {
				fprintf(stderr, "Can not rename temporary file\n");
			}
		}
		fprintf(stderr, "%s\n", e.what());
		rs = false;
	}
	// rs == 0 is error
	return !rs;
}

