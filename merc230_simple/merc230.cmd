[
	{
		"type": "param",
		"param": 0,
		"subparam": 8
	},
	{
		"type": "instant_current",
		"phase": 1
	},
	{
		"type": "instant_power",
		"phase": 1,
		"num" : "Q"
	},
	{
		"type": "get_array",
		"array_num": 0,
		"month" : 0,
		"tariff" : 0
	},
	{
		"type": "raw_request",
		"request_code": 4,
		"bytes": [
			"1", "0xFE"
		]
	}
]
