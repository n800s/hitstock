#ifndef _MERC230_H

#define _MERC230_H

#include <string>
#include "config.h"

class Merc230 {
	private:
		std::string device;
		int address;
		int fd;
		int baudrate;
	protected:
		void set_options(int vmin=0);
		void open_device();
		void close_device();
		uint8_t *add_byte(uint8_t byte, uint8_t *ptr);
		uint8_t *add_bytes(uint8_t *bytes, int nbytes, uint8_t *ptr);
		uint8_t *init_request(uint8_t address, uint8_t *buf);
		uint8_t *add_request_code(uint8_t code, uint8_t *ptr);
		uint8_t *add_password(uint8_t pass[6], uint8_t *ptr);
		int send_request(uint8_t *request, int len, uint8_t *reply=NULL, int replylen=0);
		int get_param(int param, uint8_t *reply, int len);
		int get_param(int param, int arg, uint8_t *reply, int len);
		uint32_t u32(uint8_t *bytes);
		int32_t i32(uint8_t *bytes);
	public:
		static bool debug;
		Merc230(const char *device, int address=0, int baudrate=BAUDRATE);
		~Merc230();
		int open_channel();
		int close_channel();
		int get_current_time(FILE *dfile);
		int get_regarray(FILE *dfile, int anum, int mon, int tariff=0);
		int get_power_params(FILE *dfile, int nom, int phase);
		int get_voltage_params(FILE *dfile, int phase);
		int get_current_params(FILE *dfile, int phase);
		int raw_request(FILE *dfile, int request_code, uint8_t *bytes, int byteslen);
};

#endif //_MERC230_H
