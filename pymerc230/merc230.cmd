[
	{
		"type": "param",
		"code": 17,
		"data": ["0x11"],
		"redis": "param17"
	},
	{
		"type": "mcurrent",
		"phase": 2
	},
	{
		"type": "mpower",
		"phase": 1,
		"ptype" : "Q"
	},
	{
		"type": "maximums",
		"month" : 1
	},
	{
		"type": "raw_request",
		"code": 4,
		"data": [
			"1", "0xFE"
		]
	}
]
