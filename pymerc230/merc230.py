#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os, getopt, struct, json, inspect, traceback, redis, socket, time
from pprint import pprint, pformat
import lib.merc230_lib as lib
from lib.exc import Exc230

serdev = "/dev/ttyAMA0"
baud = 9600
address = 0
cmdfname = None
cmdjson = None
rhost = 'localhost'
rport = 6379

def usage():
	print >>sys.stderr, ("merc230 -c <command json file or ->\n"
		"-h - help\n"
		"-c - command file name\n"
		"-r - Redis host:port\n"
		"-a - Mercury 230 address (default: 0)\n"
		"-d - Serial device (default: %s)\n"
		"-b - Baud rate (default: %s)\n") % (serdev, baud)

try:
	opts, args = getopt.gnu_getopt(sys.argv[1:], 'hc:a:d:b:r:', ['help'])
except getopt.GetoptError as err:
	print >>sys.stderr, str(err)
	usage()
	sys.exit(2)

for o, a in opts:
	if o in ("-h", "--help"):
		usage()
		sys.exit()
	elif o in ("-c",):
		cmdfname = a
	elif o in ("-a",):
		address = int(a)
	elif o in ("-r",):
		rhost,rport = (a.split(':') + [rport])[:2]
		rport = int(rport)
	elif o in ("-d",):
		serdev = a
	elif o in ("-b",):
		baud = int(a)
	else:
		assert False, "unhandled option"

if cmdfname:
	if cmdfname == '-':
		cmdjson = json.load(stdio)
	else:
		cmdjson = json.load(file(cmdfname))

r = redis.Redis(host=rhost, port=rport)

#print 'Run %s, %s, %d baud' % (address, serdev, baud)
dev = lib.Merc230(address, serdev, baud=baud)
#lib.DEBUG = True
dev.open_channel((1, 1, 1, 1, 1, 1,))

try:

	reply = []
	if cmdjson:
		for cmd in cmdjson:
			try:
				cmdtype = cmd['type']
				cmdargs = cmd.keys()
				cmdargs.remove('type')
				funcname = 'get_' + cmdtype
				if not hasattr(dev, funcname):
					raise Exc230('Unknown type "%s"' % cmdtype)
				func = getattr(dev, funcname)
				arglist = list(inspect.getargspec(func)[0])
				arglist.remove('self')
				argvals = []
				for arg in arglist:
					try:
						argval = cmd[arg]
						if arg == 'data':
							argval = [int(b, 0) for b in argval]
							argval = struct.pack('B' * len(argval), *argval)
					except AttributeError:
						raise Exc230('"%s" has no "%s". Needed: %s. Provided: %s' % (cmdtype, arg, ','.join(sorted(arglist)), ','.join(sorted(cmdargs))))
					argvals.append(argval)
				t = time.time()
				cmd[cmdtype] = func(*argvals)
				cmd['timestamp'] = t
				cmd['timestamp_hr'] = time.strftime('%a, %d %b %Y %H:%M:%S +0000', time.localtime(t))
				cmd['hostname'] = socket.gethostname()
#				print cmd
			except:
				exc_type, exc_value, exc_traceback = sys.exc_info()
				if not isinstance(exc_value, Exc230):
					print >>sys.stderr, 'Error while executing', json.dumps(cmd)
					traceback.print_exc(file=sys.stderr)
				cmd['error'] = '%s: %s (Command: %s)' % (exc_type.__name__, unicode(exc_value), json.dumps(cmd))
			reply.append(cmd)
			jcmd = json.dumps(cmd, ensure_ascii=False, indent=2)
			r.rpush(cmd.get("redis", "result"), jcmd)


finally:
	lib.DEBUG = False
	dev.close_channel()
