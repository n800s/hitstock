import serial, struct, array, time, json, traceback
from merc230_crc import addCRC, checkCRC
from datetime import date, datetime

DEBUG = False

E_FROM_RESET = 0
E_CUR_YEAR = 1
E_PREV_YEAR = 2
E_MONTH = 3
E_CUR_DAY = 4
E_PREV_DAY = 5
E_TOT_ACTIVE = 6
E_CUR_YEAR_BEGIN = 9
E_PREV_YEAR_BEGIN = 0xA
E_MONTH_BEGIN = 0xB
E_CUR_DAY_BEGIN = 0xC
E_PREV_DAY_BEGIN = 0xD

def print_buf(prefix, buf):
	lprint(prefix)
	for c in array.array('B', buf):
		lprint('%2.2X' % c)
	lprint('\n')

def init_request(address):
	return struct.pack('B', address)

def request_code(code):
	return struct.pack('B', code)

def lprint(*args):
	if DEBUG:
		for el in args:
			print el,

class Merc230:

	def __init__(self, address, serdev, baud=9600):
		self.serdev = serdev
		self.baud = baud
		self.address = address
		self.s = serial.Serial(self.serdev, self.baud, timeout=1, interCharTimeout=1)

	def send_request(self, reqcode, reqdata=''):
		v = addCRC(init_request(self.address) + request_code(reqcode) + reqdata)
		print_buf('Send:', v)
		self.s.write(v)
		time.sleep(0.5)
		rs = self.s.read(1000)
		print_buf('Received:', rs)
		checkCRC(rs)
		return rs

	def open_channel(self, password, access_mode=1):
		password = struct.pack('BBBBBB', *(password[:6]))
		self.send_request(1, struct.pack('B', 1) + password)
		# update apparatus description
		reply = self.read_param(0x12)
		reply = reply[1:7]
		b1,b2,b3,b4,b5,b6 = struct.unpack('BBBBBB', reply)
		self.constant = [5000, 25000, 1250, 500, 1000, 250][b2 & 0xf]

	def close_channel(self):
		self.send_request(2);

	def get_raw_request(self, code, data):
		rs = self.send_request(code, data)
		rs = struct.unpack('B' * len(rs), rs)
		return rs

	def get_regarray(self, arraynum, month, tariff):
		v = struct.pack('BB', ((arraynum & 0xf) << 4) | (month & 0xf), tariff)
		reply = self.send_request(5, v);
		replylen = len(reply) - 3
		rs = {
				'array': arraynum,
				'month': month,
				'tariff': tariff,
			}
		if replylen == 12:
			rs['A+1'],rs['A+2'],rs['A+3'] = [self.bbbb2n(reply[i:i+4], unsigned=False) for i in range(1, 13, 4)]
		elif replylen == 16:
			rs['A+'],rs['A-'],rs['R+'],rs['R-'] = [self.bbbb2n(reply[i:i+4], unsigned=False) for i in range(1, 17, 4)]
		return rs;

	def read_param(self, code, data=''):
		return self.send_request(8, struct.pack('B', code)+str(data))

	def get_param(self, code, data=''):
		rs = self.read_param(code, data)
		rs = struct.unpack('B' * len(rs), rs)
		return rs

	def bbb2n(self, bbb, unsigned=True):
		b1,b2,b3 = struct.unpack(('BBB' if unsigned else 'bBB'), bbb)
		return b1 << 16 | b3 << 8 | b2

	def bbbb2n(self, bbbb, unsigned=True):
		b1,b2,b3,b4 = struct.unpack(('BBBB' if unsigned else 'BbBB'), bbbb)
		return b2 << 24 | b1 << 16 | b4 << 8 | b3

	def get_sernum(self):
		reply = self.read_param(0)
		sn1,sn2,sn3,sn4,d,m,y = struct.unpack('BBBBBBB', reply[1:-2])
		rs = {
			'sernum': '%d%d%d%d' % (sn1, sn2, sn3, sn4),
			'date': d,
			'time_t': time.mktime(date(day=d, month=m, year=y+2000).timetuple()),
		}
		return rs

	def get_mpower(self, ptype, phase):
		ptype = ['P', 'Q', 'S'].index(ptype)
		reply = self.read_param(0x11, struct.pack('B', ((ptype & 0x3) << 2) | (phase & 0x3)))
		rs = self.bbb2n(reply[1:-2])
		Adir = -1 if rs & 0x800000 else 1
		Rdir = -1 if rs & 0x400000 else 1
		rs &= 0x3fffff
		return rs / 100., Adir, Rdir

	def get_mvoltage(self, phase):
		reply = self.read_param(0x11, struct.pack('B', 0x10 | (phase & 0x3)))
		return self.bbb2n(reply[1:-2]) / 100.

	def get_mcurrent(self, phase):
		reply = self.read_param(0x11, struct.pack('B', 0x20 | (phase & 0x3)))
		return self.bbb2n(reply[1:-2]) / 1000.

	def get_mfreq(self):
		reply = self.read_param(0x11, struct.pack('B', 0x40))
		return self.bbb2n(reply[1:-2]) / 100.

	def get_cosfi(self, ptype):
		ptype = ['1-2', '1-3', '2-3'].index(ptype) + 1
		reply = self.read_param(0x11, struct.pack('B', 0x50 | ptype))
		return self.bbb2n(reply[1:-2], unsigned=False) / 1000.

	def get_allpower(self, ptype):
		ptype = ['P', 'Q', 'S'].index(ptype)
		reply = self.read_param(0x16, struct.pack('B', ((ptype & 0x3) << 2)))
		rs = []
		for pf in struct.unpack('3s3s3s3s', reply[1:-2]):
			pw = self.bbb2n(pf)
			pw &= 0x3fffff
			rs.append(pw / 100.)
		return rs

	def get_fixed_voltage(self):
		reply = self.read_param(0x14, struct.pack('B', 0x10))
		rs = []
		for pf in struct.unpack('3s3s3s', reply[1:-2]):
			rs.append(self.bbb2n(pf)/100.)
		return rs

	def get_fixed_current(self):
		reply = self.read_param(0x14, struct.pack('B', 0x20))
		rs = []
		for pf in struct.unpack('3s3s3s', reply[1:-2]):
			rs.append(self.bbb2n(pf)/1000.)
		return rs

	def get_power_coef(self, ptype):
		ptype = ['P', 'Q', 'S'].index(ptype)
		reply = self.read_param(0x14, struct.pack('B', ((ptype & 0x3) << 2)))
		rs = []
		for pf in struct.unpack('4s4s4s4s', reply[1:-2]):
			rs.append(self.bbbb2n(pf)/100.)
		return rs

	def get_max_scheduler(self, month):
		reply = self.read_param(0xc, struct.pack('B', month))
		rs = {
				'morning': [
					[struct.unpack('B', reply[1])[0], struct.unpack('B', reply[2])[0]],
					[struct.unpack('B', reply[3])[0], struct.unpack('B', reply[4])[0]],
				],
				'evening': [
					[struct.unpack('B', reply[5])[0], struct.unpack('B', reply[6])[0]],
					[struct.unpack('B', reply[7])[0], struct.unpack('B', reply[8])[0]],
				],
		}
		return rs

	def get_maximums(self, month):
		reply = self.read_param(0xd, struct.pack('B', month))
		rs = {
			'morning': {
				'A+': struct.unpack('<H', reply[1:3])[0],
				'A-': struct.unpack('<H', reply[5:7])[0],
				'R+': struct.unpack('<H', reply[9:11])[0],
				'R-': struct.unpack('<H', reply[13:15])[0],
			},
			'evening': {
				'A+': struct.unpack('<H', reply[3:5])[0],
				'A-': struct.unpack('<H', reply[7:9])[0],
				'R+': struct.unpack('<H', reply[11:13])[0],
				'R-': struct.unpack('<H', reply[15:17])[0],
			},
		}
		return rs

	def read_memory(self, address, memnum, ecode, size):
		bit17 = bool(address & 0x10000)
		address = address & 0xffff
		code = (memnum & 0xf) | (((ecode & 0x7) << 4) if memnum == 3 else 0) | (0x80 if bit17 and memnum == 3 else 0)
		return self.send_request(6, struct.pack('>BHB', code, address, size))

	def decode_memory_reply(self, reply):
		state = struct.unpack('B', reply[1])[0]
		rs = {
			'state': state,
			'statebits': self.state2bits(state),
			'hour': int('%2.2X' % struct.unpack('B', reply[2])[0]),
			'min': int('%2.2X' % struct.unpack('B', reply[3])[0]),
			'day': int('%2.2X' % struct.unpack('B', reply[4])[0]),
			'month': int('%2.2X' % struct.unpack('B', reply[5])[0]),
			'year': int('%2.2X' % struct.unpack('B', reply[6])[0]),
			'period': struct.unpack('B', reply[7])[0],
			'P+': struct.unpack('<h', reply[8:10])[0],
			'P-': struct.unpack('<h', reply[10:12])[0],
			'Q+': struct.unpack('<h', reply[12:14])[0],
			'Q-': struct.unpack('<h', reply[14:16])[0],
		}
		for v in ('P+', 'P-', 'Q+', 'Q-'):
			rs[v] = rs[v] if rs[v] in (0, -1) else (rs[v] * 60. / 30 / (2 * self.constant))
		return rs


	def get_lastrec_params(self):
		reply = self.read_param(0x13)
		state = struct.unpack('B', reply[3])[0]
		statebits = self.state2bits(state)
		rs = {
			'address': struct.unpack('>H', reply[1:3])[0],
			'state': state,
			'statebits': statebits,
			'hour': int('%2.2X' % struct.unpack('B', reply[4])[0]),
			'min': int('%2.2X' % struct.unpack('B', reply[5])[0]),
			'day': int('%2.2X' % struct.unpack('B', reply[6])[0]),
			'month': int('%2.2X' % struct.unpack('B', reply[7])[0]),
			'year': int('%2.2X' % struct.unpack('B', reply[8])[0]),
			'period': struct.unpack('B', reply[9])[0],
		}
		return rs

	def state2bits(self, state):
		return {
			'bit17': bool(state & 0x10),
			'is_winter': bool(state & 0x8),
			'meminit': bool(state & 0x4),
			'not_full': bool(state & 0x2),
			'overload': bool(state & 0x1),
		}

	def get_avgpower_lastrecs(self, count=10):
		params = self.get_lastrec_params()
		addr = params['address']
# + (0x10000 if params['statebits']['bit17'] else 0)
		rs = []
		for i in range(count):
			d_addr = addr - 0x10 * i
			reply = self.read_memory(d_addr, 3, 0, 15)
			data = self.decode_memory_reply(reply)
			data['memory_block'] = d_addr / 0x10
			data['address'] = d_addr
			rs.append(data)
		return rs

	def get_avgpower(self, memory_block=0):
		d_addr = memory_block * 0x10
		reply = self.read_memory(d_addr, 3, 0, 15)
		rs = self.decode_memory_reply(reply)
		rs['memory_block'] = memory_block
		rs['address'] = d_addr
		return rs

