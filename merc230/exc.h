#ifndef _EXC_H

#define _EXC_H

#include <exception>
#include <cstdarg>

class merception: public std::exception
{
	public:
		inline merception(const char * format, ...) {
			va_list args;
			va_start(args, format);
			vsnprintf(error_msg, sizeof(error_msg), format, args);
			va_end(args);
		}

		inline virtual const char * what() const throw()
		{
			return error_msg;
		}

	private:
		char error_msg[256];

};


#endif //_EXC_H
