#!/bin/sh

jl=$(atq | cut -f1)
if [ -n "$jl" ]
then
	echo 'Cleaning...'
	atrm $jl
else
	echo 'Nothing to clean'
fi
