#ifndef _CONFIG_H
#define _CONFIG_H

#ifdef TION2
#define SERIAL_DEVICE "/dev/ttyAM2"
#endif

#ifdef TION28
#define SERIAL_DEVICE "/dev/ttySP2"
#endif

#ifdef PC386
#define SERIAL_DEVICE "/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0"
#endif

#ifdef PI
#define SERIAL_DEVICE "/dev/ttyAMA0"
//#define SERIAL_DEVICE "/dev/serial/by-id/usb-1a86_USB2.0-Serial-if00-port0"
#endif

#define BAUDRATE 9600

#endif //_CONFIG_H
