#include "all.h"
#include "config.h"
#include "exc.h"
#include "merc200.h"

static char serdev[256] = SERIAL_DEVICE;
static int baudrate = BAUDRATE;
static char destdir[256] = ".";
static uint32_t address = 0;
static char cmdfname[256] = "";

static char tmpfname[392];
static char destfname[392];
static FILE *dfile;


static void usage()
{
	printf("merc200 <options>\n");
	printf("-h - Help\n");
	printf("-p <Destination directory (default: current)>\n");
	printf("-a <Mercury 200 address (default: 0)>\n");
	printf("-d <Serial device (default: %s)>\n", serdev);
	printf("-b <Baud rate (default: %d)>\n", baudrate);
	printf("-c <Command file>\n");
}

static void err_usage(char arg)
{
	printf("Bad arg \"%c\"", arg);
	usage();
}

bool open_file()
{
	// create transaction file
	time_t t = time(NULL);
	char fname[128];
	strftime(fname, sizeof(fname), "__%Y_%m_%d_%H_%M_%S.json", localtime(&t));
	snprintf(tmpfname, sizeof(tmpfname), "%s/%s", destdir, fname);
	strftime(fname, sizeof(fname), "%Y_%m_%d_%H_%M_%S.json", localtime(&t));
	snprintf(destfname, sizeof(destfname), "%s/%s", destdir, fname);
//	printf("rename %s to %s\n", tmpfname, destfname);
	dfile = fopen(tmpfname, "wt");
	if(dfile) {
		fprintf(dfile, "[\n");
		fprintf(dfile, "\t{\n");
		fprintf(dfile, "\t\t\"type\": \"start\"\n");
		fprintf(dfile, "\t\t\"timestamp\": %d\n", time(NULL));
		fprintf(dfile, "\t},\n");
	}
	return dfile != NULL;
}

bool close_file()
{
	fprintf(dfile, "\t{\n");
	fprintf(dfile, "\t\"type\": \"finish\",\n");
	fprintf(dfile, "\t\t\"timestamp\": %d\n", time(NULL));
	fprintf(dfile, "\t}\n");
	fprintf(dfile, "]\n");
	fclose(dfile);
	// rename file
	return rename(tmpfname, destfname) == 0;
}

int to_integer(json::value val)
{
	int rs;
	if(is_number(val)) {
		rs = to_number(val);
	} else {
		std::stringstream converter(to_string(val));
		converter >> std::hex >> rs;
	}
	return rs;
}

// every 15 min, every day, the first day of month
int main(int argc, char* argv[])
{
	int ch;
	bool rs = false;
	while((ch = getopt(argc, argv, "p:a:d:b:c:")) != -1)
	{
		switch(ch)
		{
			case 'p':
				strncpy(destdir, optarg, sizeof(destdir) - 1);
				break;
			case 'a':
				address = atoi(optarg);
				break;
			case 'd':
				strncpy(serdev, optarg, sizeof(serdev));
				break;
			case 'b':
				baudrate = atoi(optarg);
				break;
			case 'c':
				strncpy(cmdfname, optarg, sizeof(cmdfname));
				break;
			case 'h':
				usage();
				break;
			default:
				err_usage(ch);
				break;
		}
	}

	try {
		std::ifstream cmdfile(cmdfname);
		json::value json = json::parse(cmdfile);

		Merc200 *m = new Merc200(serdev, address, baudrate);
		if(open_file()) {
				m->debug = true;
			const json::array &a = as_array(json);
			for(json::array::const_iterator it = a.begin(); it != a.end(); ++it) {
				const json::value &v = *it;
				std::string type = to_string(v["type"]);
				if( type == "instant_current" ) {
					m->get_current_params(dfile, to_number(v["phase"]));
				} else if( type == "instant_voltage" ) {
					m->get_voltage_params(dfile, to_number(v["phase"]));
				} else if ( type == "instant_power" ) {
					int inum = 0;
					std::string num = to_string(v["num"]);
					if( num == "P" ) {
						inum = 0;
					} else if( num == "Q" ) {
						inum = 1;
					} else if( num == "S" ) {
						inum = 2;
					}
					m->get_power_params(dfile, inum, to_number(v["phase"]));
				} else if ( type == "get_array" ) {
					m->get_regarray(dfile, to_number(v["array_num"]), to_number(v["month"]), to_number(v["tariff"]));
				} else if ( type == "current_time" ) {
					m->get_current_time(dfile);
				} else if ( type == "raw_request" ) {
					uint8_t *vbuf = NULL;
					int vbuflen = 0;
					if( has_key(v, "bytes") ) {
						const json::array &va = as_array(v["bytes"]);
						vbuflen = va.size();
						vbuf = new uint8_t[vbuflen + 1];
						int vi = 0;
						for(json::array::const_iterator vit = va.begin(); vit != va.end(); ++vit) {
							vbuf[vi++] = to_integer(*vit);
						}
					}
					m->raw_request(dfile, to_integer(v["request_code"]), vbuf, vbuflen);
					if( vbuf ) {
						delete vbuf;
					}
				} else {
					std::cout << "not found: " << to_string(v["type"]) << std::endl;
				}
				m->debug = false;
			}

			rs = true;
			if(!close_file()) {
				fprintf(stderr, "Can not rename temporary file\n");
				rs = false;
			}
		} else {
			fprintf(stderr, "Can not create temporary file\n");
		}
	} catch(merception &e) {
		fprintf(dfile, "{\n");
		fprintf(dfile, "\t\"type\": \"fatal_error\"\n");
		fprintf(dfile, "\t\"error\": \"%s\"\n", e.what());
		fprintf(dfile, "}\n");
		if(!close_file()) {
			fprintf(stderr, "Can not rename temporary file\n");
		}
		fprintf(stderr, "%s\n", e.what());
		rs = false;
	}
	// rs == 0 is error
	return !rs;
}

