#include <errno.h>
#include <termios.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>

#include <algorithm>

#include "config.h"
#include "exc.h"
#include "crc.h"

#include "merc200.h"

static struct constant {
	long speed;
	long value;
} termios_constants[] = {
	{50, B50},
	{75, B75},
	{110, B110},
	{134, B134},
	{150, B150},
	{200, B200},
	{300, B300},
	{600, B600},
	{1200, B1200},
	{1800, B1800},
	{2400, B2400},
	{4800, B4800},
	{9600, B9600},
	{19200, B19200},
	{38400, B38400},
#ifdef B57600
	{57600, B57600},
#endif
#ifdef B115200
	{115200, B115200},
#endif
#ifdef B200400
	{200400, B200400},
#endif
	{0, 0}
};

long speed_code(long baudrate)
{
	constant *p;
	int i = 0;
	long rs = 0;
	do {
		p = termios_constants + i;
		if(p->speed == baudrate) {
			rs = p->value;
			break;
		}
		i++;
	} while(p->speed != 0);
	if( rs == 0 ) {
		throw merception("Error. Baud rate %d is not supported\n", baudrate);
	}
	return rs;
}

//#define DEBUG

bool Merc200::debug = false;


static void print_buf(uint8_t *buf, int len, FILE *stream=stdout)
{
	for(int i=0; i<len; i++) {
		fprintf(stream, "%2.2x ", buf[i]);
	}
	fprintf(stream, "\n");
}

void Merc200::set_options(int vmin)
{
	struct termios options;
	tcgetattr(fd, &options);
	options.c_cflag |= (speed_code(baudrate) | CS8 | CLOCAL | CREAD);
	//settings stolen from pyserial
	options.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | ISIG | IEXTEN | ECHOKE | ECHOCTL);
	options.c_oflag &= ~(OPOST);
	options.c_iflag &= ~(INLCR | IGNCR | ICRNL | IGNBRK);
	options.c_cc[VMIN] = vmin;
	options.c_cc[VTIME] = 10;
//	tcflush(fd, TCIFLUSH);
//	tcsetattr(fd, TCSAFLUSH, &options);
	tcsetattr(fd, TCSANOW, &options);
//	fsync(fd);
	tcflush(fd, TCIOFLUSH);
}

Merc200::Merc200(const char *device, int address, int baudrate):
	device(device), address(address), fd(-1), baudrate(baudrate)
{
}

Merc200::~Merc200()
{

	close_device();
}

void Merc200::open_device()
{
	fd=open(device.c_str(), O_RDWR | O_NOCTTY | O_NDELAY);
	if(fd<0) {
		throw merception("Device open error %s", strerror(errno));
	}
	set_options();
}

void Merc200::close_device()
{
	if(fd>=0)
	{
		if(close(fd) < 0) {
			/* Error handling. See errno. */
			fprintf(stderr, "error closing...%s\n", strerror(errno));
		}
		fd = -1;
	}
}

uint8_t *Merc200::add_byte(uint8_t byte, uint8_t *ptr)
{
	*ptr = byte;
	return ptr + 1;
}

uint8_t *Merc200::add_bytes(uint8_t *bytes, int nbytes, uint8_t *ptr)
{
	for(int i=0; i<nbytes; i++) {
		ptr[i] = bytes[i];
	}
	return ptr + nbytes;
}

uint8_t *Merc200::init_request(uint32_t address, uint8_t *buf)
{
	printf("address=%d\n", address);
	address = u32((uint8_t*)&address);
	return add_bytes((uint8_t *)&address, 4, buf);
}

uint8_t *Merc200::add_request_code(uint8_t code, uint8_t *ptr)
{
	return add_byte(code, ptr);
}

// return number of bytes read
int Merc200::send_request(uint8_t *request, int len, uint8_t *reply, int replylen)
{
	int n, rs = 0, repeat = 10, loop=0;
	open_device();
	try {
		for(int loop=0; loop<repeat; loop++) {
			try {
				addCRCbytes(request, len);
				if(debug) {
					printf("\nSending %d: ", len+2);
					print_buf(request, len+2);
				}
//				printf("Request: %d\n", request[1]);
				n = write(fd, request, len+2);
				tcdrain(fd);
				if(debug) {
					printf("Sent %d byte(s)\n", n);
				}
				sleep(1.5);
				uint8_t rcv[300];
				n = read(fd, rcv, sizeof(rcv));
				if(debug) {
					printf("\nReply has %d bytes: ", n);
					print_buf(rcv, n);
				}
				if(n < 0) {
					throw merception("error reading...%s", strerror(errno));
				} else if( n < 3 ) {
					throw merception("Reply is too short...");
				} else {
					uint16_t crc = calcCRC(rcv, n-2);
					uint16_t reply_crc = *(rcv + n-2) << 8 | *(rcv + n-1);
					if(crc != reply_crc) {
						throw merception("CRC error. Reply size:%d. CRC:(%4.4X) vs REPLY(%4.4X)", n, crc, reply_crc);
					}
					if(reply) {
						memcpy(reply, rcv, std::min(replylen, n));
					}
					rs = n;
				}
				break;
			} catch(merception &e) {
				if(loop >= repeat - 1) {
					throw;
				} else {
					fprintf(stderr, "%s, Repeating...\n", e.what());
				}
			}
		}
		close_device();
	} catch(...) {
		close_device();
		throw;
	}
	return rs;
}

uint32_t Merc200::u32(uint8_t *bytes)
{
	return bytes[0] << 24 | bytes[1] << 16 | bytes[2] << 8 | bytes[3];
}

int32_t Merc200::i32(uint8_t *bytes)
{
	union {
		uint8_t b[4];
		int32_t v;
	} rs;
	rs.b[0] = bytes[3];
	rs.b[1] = bytes[2];
	rs.b[2] = bytes[1];
	rs.b[3] = bytes[0];
//	printf("%4.4X vs %4.4X\n", *(int32_t*)bytes, rs.v);
	return rs.v;
}

int Merc200::get_param(int param, uint8_t *reply, int len)
{
	uint8_t buf[50];
	uint8_t *ptr = init_request(address, buf);
	ptr = add_request_code(8, ptr);
	ptr = add_byte(param, ptr);
	return send_request(buf, ptr - buf, reply, len);
}

int Merc200::get_param(int param, int arg, uint8_t *reply, int len)
{
	uint8_t buf[50];
	uint8_t *ptr = init_request(address, buf);
	ptr = add_request_code(8, ptr);
	ptr = add_byte(param, ptr);
	ptr = add_byte(arg, ptr);
	return send_request(buf, ptr - buf, reply, len);
}

int Merc200::raw_request(FILE *dfile, int request_code, uint8_t *bytes, int byteslen)
{
	uint8_t buf[50];
	uint8_t *ptr = init_request(address, buf);
	ptr = add_request_code(request_code, ptr);
	ptr = add_bytes(bytes, std::min((int)sizeof(buf)-2, byteslen), ptr);
	uint8_t reply[300];
	int rs = send_request(buf, ptr - buf, reply, sizeof(reply));
	fprintf(dfile, "\t{\n");
	fprintf(dfile, "\t\t\"type\": \"raw_request\"\n");
	fprintf(dfile, "\t\t\"request\": 0x%2.2X\n", request_code);
	fprintf(dfile, "\t\t\"bytes\": [\n");
	for(int i=0; i<rs; i++) {
		fprintf(dfile, "\t\t\t\"0x%2.2X\"", reply[i]);
		if(i<rs) {
			fprintf(dfile, ",\n");
		} else {
			fprintf(dfile, "\n");
		}
	}
	fprintf(dfile, "\t\t]\n");
	fprintf(dfile, "\t},\n");
	return rs;
}

int Merc200::get_current_time(FILE *dfile)
{
	uint8_t buf[50];
	uint8_t *ptr = init_request(address, buf);
	ptr = add_request_code(4, ptr);
	ptr = add_byte(0, ptr);
	uint8_t reply[10];
	int rs = send_request(buf, ptr - buf, reply, sizeof(reply));
	fprintf(dfile, "\t{\n");
	fprintf(dfile, "\t\t\"type\": \"current_time\"\n");
	fprintf(dfile, "\t\t\"sec\": %X\n", reply[1]);
	fprintf(dfile, "\t\t\"min\": %X\n", reply[2]);
	fprintf(dfile, "\t\t\"hour\": %X\n", reply[3]);
	fprintf(dfile, "\t\t\"weekday\": %X\n", reply[4]);
	fprintf(dfile, "\t\t\"day\": %X\n", reply[5]);
	fprintf(dfile, "\t\t\"month\": %X\n", reply[6]);
	fprintf(dfile, "\t\t\"year\": %X\n", reply[7]);
	fprintf(dfile, "\t\t\"winter\": %X\n", reply[8]);
	fprintf(dfile, "\t},\n");
	return rs;
}

int Merc200::get_regarray(FILE *dfile, int anum, int mon, int tariff)
{
	uint8_t buf[50];
	uint8_t *ptr = init_request(address, buf);
	ptr = add_request_code(5, ptr);
	ptr = add_byte(((anum & 0xf) << 4) | (mon & 0xf), ptr);
	ptr = add_byte(tariff, ptr);
	uint8_t reply[30];
	int rs = send_request(buf, ptr - buf, reply, sizeof(reply));
	switch(rs-3) {
		case 12:
			fprintf(dfile, "\t{\n");
			fprintf(dfile, "\t\t\"type\": \"array\"\n");
			fprintf(dfile, "\t\t\"tariff\": %d\n", tariff);
			fprintf(dfile, "\t\t\"month\": %d\n", mon);
			fprintf(dfile, "\t\t\"array\": %d\n", anum);
			fprintf(dfile, "\t\t\"Ap1\": %u\n", u32(reply+1));
			fprintf(dfile, "\t\t\"Ap2\": %u\n", u32(reply+5));
			fprintf(dfile, "\t\t\"Ap3\": %u\n", u32(reply+9));
			fprintf(dfile, "\t},\n");
			break;
		case 16:
			fprintf(dfile, "\t{\n");
			fprintf(dfile, "\t\t\"type\": \"array\"\n");
			fprintf(dfile, "\t\t\"tariff\": %d\n", tariff);
			fprintf(dfile, "\t\t\"month\": %d\n", mon);
			fprintf(dfile, "\t\t\"array\": %d\n", anum);
			fprintf(dfile, "\t\t\"Ap\": %d\n", i32(reply+1));
			fprintf(dfile, "\t\t\"Am\": %d\n", i32(reply+5));
			fprintf(dfile, "\t\t\"Rp\": %d\n", i32(reply+9));
			fprintf(dfile, "\t\t\"Rm\": %d\n", i32(reply+13));
			fprintf(dfile, "\t},\n");
			break;
	}
	return rs;
}

static const char *nom_str(int index)
{
	static const char *noms[] = {
		"P",
		"Q",
		"S"
	};
	if(index >= 0 && index <= 2) {
		return noms[index];
	} else {
		return "unknown";
	}
}

static const char *phase_str(int index)
{
	static const char *phases[] = {
		"total",
		"1",
		"2",
		"3"
	};
	if(index >= 0 && index <= 3) {
		return phases[index];
	} else {
		return "unknown";
	}
}

int Merc200::get_power_params(FILE *dfile, int nom, int phase)
{
	uint8_t arg = ((nom & 0x3) << 2) | (phase & 0x3);
	uint8_t reply[50];
	int rs = get_param(0x11, arg, reply, sizeof(reply));
	bool Ap_straight = !bool(reply[1] & 0x8);
	bool Rp_straight = !bool(reply[1] & 0x4);
	fprintf(dfile, "\t{\n");
	fprintf(dfile, "\t\t\"type\": \"instant_power\"\n");
	fprintf(dfile, "\t\t\"Ap\": \"%s\"\n", (Ap_straight)? "straight" : "reverse");
	fprintf(dfile, "\t\t\"Rp\": \"%s\"\n", (Rp_straight)? "straight" : "reverse");
	fprintf(dfile, "\t\t\"power\": \"%s\"\n", nom_str(nom));
	fprintf(dfile, "\t\t\"phase\": \"%s\"\n", phase_str(phase));
	fprintf(dfile, "\t\t\"V1\": %d\n", reply[2]);
	fprintf(dfile, "\t\t\"V2\": %d\n", reply[3]);
	fprintf(dfile, "\t},\n");
	return rs;
}

int Merc200::get_voltage_params(FILE *dfile, int phase)
{
	uint8_t arg = 0x10 | (phase & 0x3);
	uint8_t reply[20];
	int rs = get_param(0x11, arg, reply, sizeof(reply));
	fprintf(dfile, "\t{\n");
	fprintf(dfile, "\t\t\"type\": \"instant_voltage\"\n");
	fprintf(dfile, "\t\t\"phase\": \"%s\"\n", phase_str(phase));
	fprintf(dfile, "\t\t\"V\": %d\n", (reply[1] << 16)|(reply[3] << 8)|(reply[2]));
	fprintf(dfile, "\t},\n");
	return rs;
}

int Merc200::get_current_params(FILE *dfile, int phase)
{
	uint8_t arg = 0x20 | (phase & 0x3);
	uint8_t reply[20];
	int rs = get_param(0x11, arg, reply, sizeof(reply));
	fprintf(dfile, "\t{\n");
	fprintf(dfile, "\t\t\"type\": \"instant_current\"\n");
	fprintf(dfile, "\t\t\"phase\": \"%s\"\n", phase_str(phase));
	fprintf(dfile, "\t\t\"A\": %d\n", (reply[1] << 16)|(reply[3] << 8)|(reply[2]));
	fprintf(dfile, "\t},\n");
	return rs;
}

